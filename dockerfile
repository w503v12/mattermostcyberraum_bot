FROM python:3.7

RUN mkdir -p /etc/mattermostcyberraum_bot

WORKDIR /etc/mattermostcyberraum_bot/

COPY requirements.txt ./
RUN /usr/local/bin/pip3.7 install -r requirements.txt
RUN apt update
RUN apt install -y poppler-utils

COPY ./mattermostcyberraum_bot/ /etc/mattermostcyberraum_bot
RUN rm -f /usr/local/lib/python3.7/site-packages/mmpy_bot/plugins/help.py
COPY ./plugins/help.py /usr/local/lib/python3.7/site-packages/mmpy_bot/plugins/
COPY /dispatcher.py /usr/local/lib/python3.7/site-packages/mmpy_bot
COPY ./mattermostcyberraum_bot/Files /etc/mattermostcyberraum_bot/
COPY ./plugins /usr/local/lib/python3.7/site-packages/mmpy_bot/plugins/
RUN rm -f /usr/local/lib/python3.7/site-packages/mmpy_bot/dispatcher.py
RUN rm -f /usr/local/lib/python3.7/site-packages/mmpy_bot/plugins/busy.py
COPY ./dispatcher.py /usr/local/lib/python3.7/site-packages/mmpy_bot/dispatcher.py

CMD [ "/usr/local/bin/python3.7", "/etc/mattermostcyberraum_bot/run.py" ]