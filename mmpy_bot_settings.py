SSL_VERIFY = True  # Whether to perform SSL cert verification
BOT_URL = '%%BOTURL%%'  # with 'http://' and with '/api/v4' path. without trailing slash.
BOT_LOGIN = '%%%BOTMAIL%%'
BOT_PASSWORD = '%%BOTPW%%'
BOT_TOKEN = None # or '<bot-personal-access-token>' if you have set bot personal access token.
BOT_TEAM = '%%BOTTEAM%%'  # possible in lowercase
WEBHOOK_ID = '%%WEBHOOKID%%' # otherwise the bot will attempt to create one