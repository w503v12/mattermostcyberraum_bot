from mmpy_bot.bot import respond_to
from pdf2image import convert_from_path, convert_from_bytes
import requests
import datetime
import os

weeknumber = datetime.date.today().isocalendar()[1]
yearnumber = datetime.date.today().year

if weeknumber < 10:
    weeknumber = '0'+ str(weeknumber)

    # after that use str(datetime.date.today().isocalendar()[1])



def timeStamped(fname, fmt='%d-%m-%Y %H-%M-%S_{fname}'):
    return datetime.datetime.now().strftime(fmt).format(fname=fname)

@respond_to('menus')
def menus_reply(message):
    # upload_file() can upload only one file at a time
    # If you have several files to upload, you need call this function several times.

    dest = '/etc/mattermostcyberraum_bot/Files/Menu/menu.jpg'
    pdffile = '/etc/mattermostcyberraum_bot/Files/Menu/Menu.pdf'
    urls = ['https://www.campusberlinbuch.de/pdf/Speiseplan.pdf','https://www.helios-gesundheit.de/fileadmin/UWS_Kliniken/Klinikum_Berlin-Buch/Ihr_Aufenthalt/Waehrend_des_Aufenthalts/Cafeteria/KW'+str(weeknumber)+'_'+str(yearnumber)+'_HKBB_Speisenplan_Cafeteria.pdf']

    for url in urls:
        res = requests.get(url,verify=False)
        open(pdffile, 'wb').write(res.content)

        images = convert_from_path(pdffile)

        for image in images:
            image.save(dest,'JPEG')
            file = open(dest,'rb')
            result = message.upload_file(file)
            file.close()
            file_id = result['file_infos'][0]['id']
            message.reply('Here are the current menus:' + " ",[file_id])

menus_reply.__doc__ = "Send menus for MDC and Helios"