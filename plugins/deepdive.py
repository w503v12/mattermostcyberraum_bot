# -*- coding: utf-8 -*-

import re

from mmpy_bot.bot import listen_to
from mmpy_bot.bot import respond_to

@respond_to('^deepdive$', re.IGNORECASE)
def deepdive_reply(message):
    message.reply('You are probably looking for this: https://tinder.com')


deepdive_reply.__doc__ = "Send tinder"