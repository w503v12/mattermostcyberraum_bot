#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', ]

setup_requirements = [ ]

test_requirements = [ ]

setup(
    author="Jan-Eric Steinhübel",
    author_email='Jan-Eric.Steinhuebel@tutanota.de',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="A small bot to entertain the cyberraum community",
    entry_points={
        'console_scripts': [
            'mattermostcyberraum_bot=mattermostcyberraum_bot.cli:main',
        ],
    },
    install_requires=['requirements','datetime','mmpy_bot','re','pdf2image','requests'],
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='mattermostcyberraum_bot',
    name='mattermostcyberraum_bot',
    packages=find_packages(include=['mattermostcyberraum_bot']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://github.com/w503v12/mattermostcyberraum_bot',
    version='0.1.0',
    zip_safe=False,
)
