=======================
MatterMostCyberRaum_Bot
=======================


.. image:: https://img.shields.io/pypi/v/mattermostcyberraum_bot.svg
        :target: https://pypi.python.org/pypi/mattermostcyberraum_bot

.. image:: https://img.shields.io/travis/w503v12/mattermostcyberraum_bot.svg
        :target: https://travis-ci.org/w503v12/mattermostcyberraum_bot

.. image:: https://readthedocs.org/projects/mattermostcyberraum-bot/badge/?version=latest
        :target: https://mattermostcyberraum-bot.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status




A small bot to entertain the cyberraum community


* Free software: MIT license
* Documentation: https://mattermostcyberraum-bot.readthedocs.io.


Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
